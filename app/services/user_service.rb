class UserService < ApplicationService
 attr_reader :users
  
  def initialize(users)
    @users = users
  end

  def call
    rendered_string
  end  

  def rendered_string
    @rendered_string ||= ApplicationController.render(:xlsx,
     template: 'users/index.xlsx.axlsx',
     assigns: { users: users }
     )
  end  

end    