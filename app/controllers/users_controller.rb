class UsersController < ApplicationController
  def index
    users = User.all
    result = UserService.call(users)

    send_data result, type: Mime[:xlsx], filename: 'users.xlsx'
  end
end    