Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
 # resources :users, constraints: lambda { |req| req.format == :xlsx }
#  constraints format: :xlsx do
    resources :users
#  end
  root 'users#index'
end
